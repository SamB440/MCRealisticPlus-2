package net.islandearth.mcrealistic;

import co.aikar.commands.PaperCommandManager;
import com.convallyria.languagy.api.language.Translator;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.islandearth.mcrealistic.api.IMCRealisticAPI;
import net.islandearth.mcrealistic.api.MCRealisticAPI;
import net.islandearth.mcrealistic.commands.Fatigue;
import net.islandearth.mcrealistic.commands.MCRealisticCommand;
import net.islandearth.mcrealistic.commands.Thirst;
import net.islandearth.mcrealistic.gamemode.CustomGameMode;
import net.islandearth.mcrealistic.items.Items;
import net.islandearth.mcrealistic.listeners.BlockListener;
import net.islandearth.mcrealistic.listeners.ConnectionListener;
import net.islandearth.mcrealistic.listeners.ConsumeListener;
import net.islandearth.mcrealistic.listeners.EntityListener;
import net.islandearth.mcrealistic.listeners.FoodChangeListener;
import net.islandearth.mcrealistic.listeners.InteractListener;
import net.islandearth.mcrealistic.listeners.InventoryListener;
import net.islandearth.mcrealistic.listeners.MoveListener;
import net.islandearth.mcrealistic.listeners.ProjectileListener;
import net.islandearth.mcrealistic.listeners.RespawnListener;
import net.islandearth.mcrealistic.managers.MCRealisticManagers;
import net.islandearth.mcrealistic.placeholders.MCRealisticPlaceholders;
import net.islandearth.mcrealistic.player.MCRealisticCache;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import net.islandearth.mcrealistic.player.PlayerListener;
import net.islandearth.mcrealistic.tasks.CosyTask;
import net.islandearth.mcrealistic.tasks.DiseaseTask;
import net.islandearth.mcrealistic.tasks.FatigueTask;
import net.islandearth.mcrealistic.tasks.MiscTask;
import net.islandearth.mcrealistic.tasks.OldAdventureUpdateTask;
import net.islandearth.mcrealistic.tasks.StaminaTask;
import net.islandearth.mcrealistic.tasks.ThirstTask;
import net.islandearth.mcrealistic.tasks.TorchTask;
import net.islandearth.mcrealistic.tasks.WarmTask;
import net.islandearth.mcrealistic.tasks.WindTask;
import net.islandearth.mcrealistic.translation.Translations;
import net.islandearth.mcrealistic.utils.BlockUtils;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Tag;
import org.bukkit.World;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

public class MCRealistic extends JavaPlugin implements IMCRealisticAPI {

    private Translator translator;
    private List<World> worlds = new ArrayList<>();
    private List<UUID> colds = new ArrayList<>();
    private List<UUID> diseases = new ArrayList<>();
    private MCRealisticManagers managers;
    private MCRealisticCache cache;

    @Override
    public Translator getTranslator() {
        return translator;
    }

    public List<World> getWorlds() {
        return worlds;
    }

    public List<UUID> getColds() {
        return colds;
    }

    public List<UUID> getDiseases() {
        return diseases;
    }

    public MCRealisticManagers getManagers() {
        return managers;
    }

    public MCRealisticCache getCache() {
        return cache;
    }

    @Override
    public void onEnable() {
        MCRealisticAPI.setAPI(this);

        createFiles();
        hookPlugins();
        this.managers = new MCRealisticManagers(this);
        this.cache = new MCRealisticCache();
        this.translator = Translator.of(this).debug(debug());
        registerListeners();
        registerCommands();
        registerRecipes();
        startTasks();

        new Metrics(this, 2300);

        for (String s : getConfig().getStringList("Worlds")) {
            World w = Bukkit.getWorld(s);
            if (w != null) worlds.add(w);
        }

        // Load existing online players (reload)
        Bukkit.getOnlinePlayers().forEach(player -> {
            File file = new File(this.getDataFolder() + File.separator + "players" + File.separator + player.getUniqueId() + ".json");
            if (!file.exists()) {
                cache.addPlayer(player);
            } else {
                try {
                    Reader reader = new FileReader(file);
                    MCRealisticPlayer mcRealisticPlayer = getGson().fromJson(reader, MCRealisticPlayer.class);
                    cache.addPlayer(mcRealisticPlayer);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onDisable() {
        this.getLogger().info("Disabling...");
        Bukkit.getOnlinePlayers().forEach(player -> cache.getPlayer(player).ifPresent(account -> account.save(this)));

        translator.close();

        // Do this to solve some reload issues.
        this.translator = null;
        this.worlds = new ArrayList<>();
        this.colds = new ArrayList<>();
        this.diseases = new ArrayList<>();
        MCRealisticAPI.setAPI(null);
    }

    private void createFiles() {
        File folder = new File(this.getDataFolder() + File.separator + "players");
        if (!folder.exists()) folder.mkdirs();
        createConfig();
        Translations.generateLang(this);
    }

    private void createConfig() {
        //TODO makes classes to manage config
        File file = new File("plugins/MCRealistic-2/config.yml");
        if (!file.exists()) {

            List<String> worlds = new ArrayList<>();
            worlds.add("world");

            getConfig().options().copyDefaults(true);

            String header;
            String eol = System.getProperty("line.separator");
            header = "MCRealistic-2 Properties:" + eol;
            header += eol;
            header += "Worlds" + eol;
            header += "  Here you define which worlds you enable." + eol;
            header += eol;
            header += "WeatherAffectsPlayer" + eol;
            header += "  This option defines whether the player should be affected by the weather. Default true." + eol;
            header += eol;
            header += "Thirst" + eol;
            header += "  This option defines whether thirst is enabled. Default true." + eol;
            header += eol;
            header += "DisplayHungerMessage" + eol;
            header += "  Whether the hunger message should be shown. Default true." + eol;
            header += eol;
            header += "Cosy" + eol;
            header += "  DisplayMessage" + eol;
            header += "  Whether the cozy message should be shown. Default true." + eol;
            header += eol;
            header += "  CallInterval" + eol;
            header += "    Must be greater than 0" + eol;
            header += "    A value of 1 = 30 seconds, 2 = 60 seconds, ect..." + eol;
            header += eol;
            header += "  FoodLevelForRegeneration" + eol;
            header += "    Minimum food level to regenerate health. 0 - 20" + eol;
            header += eol;
            header += "  FoodUseToRecoverHealth" + eol;
            header += "    Food consumed in recovering heart 0 - 20" + eol;
            header += eol;
            header += "  TorchRadius" + eol;
            header += "    Distance from torch to feel \"Cosy\"" + eol;
            header += eol;
            header += "  TorchDuration" + eol;
            header += "    1 = 1/2 a heart recovery" + eol;
            header += eol;
            header += "  FurnaceRadius" + eol;
            header += "    Distance from furnace to feel \"Cosy\"" + eol;
            header += eol;
            header += "  FurnaceDuration" + eol;
            header += "    2 = 1 heart recovery" + eol;
            header += eol;
            header += "  FurnaceMustBeLit" + eol;
            header += "    The furnace has to be lit to feel \"Cosy\"" + eol;
            header += eol;
            header += "  InclementWeatherHeatReduction" + eol;
            header += "    % of distance to heat to feel \"Cosy\" in rain/snow. 0.0 - 1.0" + eol;
            header += "    0.3 = 30% of TorchRadius/FurnaceRadius" + eol;

            header += eol;
            header += "DisplayHurtMessage" + eol;
            header += "  Whether the hurt message would be shown. Default true." + eol;
            header += eol;
            header += "Weight" + eol;
            header += "  This option defines whether the player should be affected by weight." + eol;
            header += eol;
            header += "Realistic_Building" + eol;
            header += "  This option defines whether blocks will fall." + eol;
            header += eol;
            header += "Messages.Type" + eol;
            header += "  MESSAGE, ACTIONBAR, TITLE" + eol;
            header += eol;
            getConfig().options().header(header);

            getConfig().addDefault("developer.debug", false);
            getConfig().addDefault("settings.integration.name", getIntegration());
            getConfig().addDefault("Worlds", worlds);
            getConfig().addDefault("Server.Weather.WeatherAffectsPlayer", true);
            getConfig().addDefault("Server.Player.Wind.Enabled", true);
            getConfig().addDefault("Server.Player.Wind.Minimum Y", 150);
            getConfig().addDefault("Server.Player.Wind.Push", true);
            getConfig().addDefault("Server.World.Falling_Trees", true);
            getConfig().addDefault("Server.Stamina.Enabled", true);
            getConfig().addDefault("Server.Player.DisplayHungerMessage", true);
            getConfig().addDefault("Server.Player.Cosy.DisplayMessage", true);
            getConfig().addDefault("Server.Player.Cosy.CallInterval", 1);
            getConfig().addDefault("Server.Player.Cosy.FoodLevelForRegeneration", 19);
            getConfig().addDefault("Server.Player.Cosy.FoodUseToRecoverHealth", 3);
            getConfig().addDefault("Server.Player.Cosy.TorchRadius", 1.75);
            getConfig().addDefault("Server.Player.Cosy.TorchDuration", 1);
            getConfig().addDefault("Server.Player.Cosy.CampfireRadius", 12);
            getConfig().addDefault("Server.Player.Cosy.CampfireDuration", 4);
            getConfig().addDefault("Server.Player.Cosy.CampfireMustBeLit", true);
            getConfig().addDefault("Server.Player.Cosy.FurnaceRadius", 4);
            getConfig().addDefault("Server.Player.Cosy.FurnaceDuration", 2);
            getConfig().addDefault("Server.Player.Cosy.FurnaceMustBeLit", true);
            getConfig().addDefault("Server.Player.Cosy.InclementWeatherHeatReduction", 0.5f);
            getConfig().addDefault("Server.Player.Cosy.Holding Torch", true);
            getConfig().addDefault("Server.Player.DisplayHurtMessage", true);
            getConfig().addDefault("Server.Player.Weight.Enabled", true);
            getConfig().addDefault("Server.Player.Broken_Bones.Enabled", true);
            getConfig().addDefault("Server.Building.Realistic_Building", true);
            List<String> ignoredBlocks = new ArrayList<>();
            Collections.addAll(ignoredBlocks, "TORCH", "WALL_TORCH", "REDSTONE_WALL_TORCH", "REDSTONE_TORCH");
            Tag.SIGNS.getValues().forEach(material -> ignoredBlocks.add(material.toString()));
            Tag.STANDING_SIGNS.getValues().forEach(material -> ignoredBlocks.add(material.toString()));
            Tag.WALL_SIGNS.getValues().forEach(material -> ignoredBlocks.add(material.toString()));
            Tag.BUTTONS.getValues().forEach(material -> ignoredBlocks.add(material.toString()));
            Tag.FENCES.getValues().forEach(material -> ignoredBlocks.add(material.toString()));
            Tag.FENCE_GATES.getValues().forEach(material -> ignoredBlocks.add(material.toString()));
            Tag.SLABS.getValues().forEach(material -> ignoredBlocks.add(material.toString()));
            Tag.LOGS.getValues().forEach(material -> ignoredBlocks.add(material.toString()));
            Tag.CLIMBABLE.getValues().forEach(material -> ignoredBlocks.add(material.toString()));
            getConfig().addDefault("Server.Building.Ignored_Blocks", ignoredBlocks);
            getConfig().addDefault("Server.Player.Trail.Grass_Blocks", Collections.singletonList(
                    "DIRT"));
            getConfig().addDefault("Server.Player.Trail.Sand_Blocks", Collections.singletonList(
                    "SANDSTONE"));
            getConfig().addDefault("Server.Player.Trail.Dirt_Blocks", Collections.singletonList(
                    "DIRT_PATH"));
            getConfig().addDefault("Server.Player.Trail.Path_Blocks", Collections.singletonList(
                    "COBBLESTONE"));
            getConfig().addDefault("Server.Player.Trail.Enabled", true);
            getConfig().addDefault("Server.Player.Allow Fatigue", true);
            getConfig().addDefault("Server.Player.Max Fatigue", 240);
            getConfig().addDefault("Server.Player.Max Thirst", 200);
            getConfig().addDefault("Server.Player.Fatigue Tired Range Min", 150);
            getConfig().addDefault("Server.Player.Fatigue Tired Range Max", 200);
            getConfig().addDefault("Server.Player.Allow Chop Down Trees With Hands", false);
            getConfig().addDefault("Server.Player.Chop With Hands Damage", 0);
            getConfig().addDefault("Server.Player.Trees have random number of drops", true);
            getConfig().addDefault("Server.Player.Allow /fatigue", true);
            getConfig().addDefault("Server.Player.Allow /thirst", true);
            getConfig().addDefault("Server.Player.Spawn with items", true);
            getConfig().addDefault("Server.Player.Allow Enchanted Arrow", true);
            getConfig().addDefault("Server.Player.Torch_Burn", true);
            getConfig().addDefault("Server.Player.Raw_Food_Illness", true);
            getConfig().addDefault("Server.Messages.Type", "CHAT");
            getConfig().addDefault("Server.Messages.Respawn", true);
            getConfig().addDefault("Server.Player.Thirst.Interval", 6000);
            getConfig().addDefault("Server.Player.Thirst.Enabled", true);
            getConfig().addDefault("Server.Player.Thirst.Require_Purify", true);
            getConfig().addDefault("Server.Player.Immune_System.Interval", 6000);
            getConfig().addDefault("Server.Player.Immune_System.Enabled", true);
            getConfig().addDefault("Server.Player.Immune_System.Req_Players", 2);
            getConfig().addDefault("Server.GameMode.Type", "NORMAL");
            getConfig().addDefault("Server.GameMode.Custom.Ignored_Blocks", BlockUtils.getDefaultIgnore());
            getConfig().addDefault("Server.Player.Weight.Netherite", 3);
            getConfig().addDefault("Server.Player.Weight.Diamond", 3);
            getConfig().addDefault("Server.Player.Weight.Leather", 0);
            getConfig().addDefault("Server.Player.Weight.Chainmail", 1);
            getConfig().addDefault("Server.Player.Weight.Iron", 2);
            getConfig().addDefault("Server.Player.Weight.Gold", 1);
            saveConfig();
        }
    }

    private void hookPlugins() {
        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
            this.getLogger().info("PlaceholderAPI found!");
            new MCRealisticPlaceholders(this).register();
        } else {
            this.getLogger().info("PlaceholderAPI not found!");
        }
    }

    private void registerListeners() {
        PluginManager pm = Bukkit.getPluginManager();
        pm.registerEvents(new InteractListener(this), this);
        pm.registerEvents(new ConnectionListener(this), this);
        pm.registerEvents(new RespawnListener(this), this);
        pm.registerEvents(new ProjectileListener(this), this);
        pm.registerEvents(new BlockListener(this), this);
        pm.registerEvents(new MoveListener(this), this);
        pm.registerEvents(new ConsumeListener(this), this);
        pm.registerEvents(new InventoryListener(this), this);
        pm.registerEvents(new FoodChangeListener(this), this);
        pm.registerEvents(new EntityListener(this), this);
        pm.registerEvents(new PlayerListener(this), this);
    }

    private void registerCommands() {
        PaperCommandManager manager = new PaperCommandManager(this);
        manager.getCommandCompletions().registerAsyncCompletion("items", context -> {
            return ImmutableList.of("medicine", "bandage", "chocolatemilk", "purifiedwater", "hatchet");
        });
        manager.enableUnstableAPI("help");
        manager.registerCommand(new Fatigue(this));
        manager.registerCommand(new Thirst(this));
        manager.registerCommand(new MCRealisticCommand(this));
    }

    private void registerRecipes() {
        //TODO only enable certain ones depending on settings

        if (getConfig().getBoolean("Server.Player.Immune_System.Enabled")) {
            ShapelessRecipe medicinecraft = new ShapelessRecipe(new NamespacedKey(this, getDescription().getName() + "-medicine"), Items.MEDICINE.apply(null));
            medicinecraft.addIngredient(Material.GLASS_BOTTLE);
            medicinecraft.addIngredient(Material.APPLE);
            medicinecraft.addIngredient(Material.SPIDER_EYE);
            Bukkit.getServer().addRecipe(medicinecraft);
        }

        if (getConfig().getBoolean("Server.Player.Broken_Bones.Enabled")) {
            ShapelessRecipe bandageRecipe = new ShapelessRecipe(new NamespacedKey(this, getDescription().getName() + "-bandage"), Items.BANDAGE.apply(null));
            bandageRecipe.addIngredient(Material.BONE_MEAL);
            bandageRecipe.addIngredient(Material.PAPER);
            Bukkit.getServer().addRecipe(bandageRecipe);
        }

        ShapelessRecipe axe = new ShapelessRecipe(new NamespacedKey(this, getDescription().getName() + "-hatchet"), Items.HATCHET.apply(null));
        axe.addIngredient(Material.STICK);
        axe.addIngredient(Material.FLINT);
        axe.addIngredient(Material.FLINT);
        Bukkit.getServer().addRecipe(axe);

        if (getConfig().getBoolean("Server.Player.Thirst.Require_Purify")) {
            FurnaceRecipe purified = new FurnaceRecipe(new NamespacedKey(this, getDescription().getName() + "-purified"), Items.PURIFIED_WATER.apply(null), Material.POTION, 1, 150);
            Bukkit.getServer().addRecipe(purified);
        }
    }

    private void startTasks() {
        if (getConfig().getBoolean("Server.Stamina.Enabled")) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new StaminaTask(this), 0L, 60L);
        }

        if (getConfig().getBoolean("Server.Player.Torch_Burn")) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new TorchTask(this), 0L, 20L);
        }

        if (getConfig().getBoolean("Server.Player.Wind.Enabled")) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new WindTask(this), 0L, 100L);
        }

        if (getConfig().getBoolean("Server.Player.Allow Fatigue")) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new FatigueTask(this), 0L, 100L);
        }

        if (getConfig().getBoolean("Server.Player.Cosy.DisplayMessage")) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new CosyTask(this), 0L, 600L);
        }

        if (getConfig().getBoolean("Server.Player.Thirst.Enabled")) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new ThirstTask(this), 0L, getConfig().getInt("Server.Player.Thirst.Interval"));
        }

        if (getConfig().getBoolean("Server.Player.Immune_System.Enabled")) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new DiseaseTask(this), 0L, getConfig().getInt("Server.Player.Immune_System.Interval"));
        }

        CustomGameMode gamemode = CustomGameMode.valueOf(getConfig().getString("Server.GameMode.Type").toUpperCase());
        if (gamemode == CustomGameMode.PRE_ADVENTURE) {
            Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new OldAdventureUpdateTask(this), 0L, 1L);
        }

        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new MiscTask(this), 0L, 400L);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new WarmTask(this), 0L, 1L);
    }

    public Gson getGson() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .serializeNulls().create();
    }

    @NotNull
    private String getIntegration() {
        if (Bukkit.getPluginManager().getPlugin("WorldGuard") != null) {
            return "WorldGuard";
        }
        return "Default";
    }

    public boolean debug() {
        return getConfig().getBoolean("debug");
    }

    public void debug(String debug) {
        this.debug(debug, Level.INFO);
    }

    public void debug(String debug, Level level) {
        if (debug()) this.getLogger().log(level, "[Debug] " + debug);
    }
}
