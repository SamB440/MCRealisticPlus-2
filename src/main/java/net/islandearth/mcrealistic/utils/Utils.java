/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.utils;

import net.islandearth.mcrealistic.MCRealistic;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static List<Block> getNearbyBlocks(Location location, double radius) {
        List<Block> blocks = new ArrayList<>();
        for (double x = location.getBlockX() - radius; x <= location.getBlockX() + radius; x++) {
            for (double y = location.getBlockY() - radius; y <= location.getBlockY() + radius; y++) {
                for (double z = location.getBlockZ() - radius; z <= location.getBlockZ() + radius; z++) {
                    blocks.add(location.getWorld().getBlockAt((int) x, (int) y, (int) z));
                }
            }
        }

        return blocks;
    }

    public static boolean isWorldEnabled(World world) {
        MCRealistic plugin = JavaPlugin.getPlugin(MCRealistic.class);
        for (World worlds : plugin.getWorlds()) {
            if (worlds.getUID().equals(world.getUID())) {
                plugin.debug("World to check (name, uuid): " + worlds.getName() + " " + worlds.getUID());
                plugin.debug("World to compare (name, uuid): " + world.getName() + " " + world.getUID());
                return true;
            }
        }
        return false;
    }
}
