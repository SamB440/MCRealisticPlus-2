package net.islandearth.mcrealistic.utils;

import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.entity.Player;

public class TitleManager {

    public static void sendTitle(Player player, String msgTitle, String msgSubTitle, int ticks) {
        player.sendTitle(msgTitle, msgSubTitle, 20, ticks, 20);
    }

    public static void sendActionBar(Player player, String message) {
        player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(message));
    }
}
