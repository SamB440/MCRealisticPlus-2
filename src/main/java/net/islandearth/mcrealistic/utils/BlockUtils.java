package net.islandearth.mcrealistic.utils;

import com.google.common.collect.Lists;
import net.islandearth.mcrealistic.MCRealistic;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class BlockUtils {

    public static boolean isStrong(ItemStack item, Block target) {
        return target.isPreferredTool(item);
    }

    private static MCRealistic getPlugin() {
        return JavaPlugin.getPlugin(MCRealistic.class);
    }

    public static List<Material> getIgnored() {
        List<Material> materials = new ArrayList<>();
        for (String material : getPlugin().getConfig().getStringList("Server.GameMode.Custom.Ignored_Blocks")) {
            materials.add(Material.valueOf(material));
        }
        return materials;
    }

    public static List<String> getDefaultIgnore() {
        List<String> materials = Lists.newArrayList(
                Material.BROWN_MUSHROOM.toString(),
                Material.RED_MUSHROOM.toString(),
                Material.END_ROD.toString(),
                Material.REDSTONE.toString(),
                Material.REPEATER.toString(),
                Material.COMPARATOR.toString(),
                Material.LEVER.toString(),
                Material.SEA_PICKLE.toString(),
                Material.FIRE.toString(),
                Material.REDSTONE_TORCH.toString(),
                Material.REDSTONE_WALL_TORCH.toString(),
                Material.TORCH.toString(),
                Material.WALL_TORCH.toString(),
                Material.GRAVEL.toString(),
                Material.SNOW.toString(),
                Material.SNOW_BLOCK.toString());
        Tag.BEDS.getValues().forEach(material -> materials.add(material.toString()));
        Tag.LEAVES.getValues().forEach(material -> materials.add(material.toString()));
        Tag.CROPS.getValues().forEach(material -> materials.add(material.toString()));
        Tag.CARPETS.getValues().forEach(material -> materials.add(material.toString()));
        Tag.FLOWERS.getValues().forEach(material -> materials.add(material.toString()));
        return materials;
    }
}
