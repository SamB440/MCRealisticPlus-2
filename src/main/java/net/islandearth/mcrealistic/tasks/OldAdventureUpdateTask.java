package net.islandearth.mcrealistic.tasks;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.utils.BlockUtils;
import net.islandearth.mcrealistic.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class OldAdventureUpdateTask implements Runnable {

    private final MCRealistic plugin;

    public OldAdventureUpdateTask(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!Utils.isWorldEnabled(player.getWorld())) continue;
            if (player.getGameMode() != GameMode.ADVENTURE && player.getGameMode() != GameMode.SURVIVAL) continue;
            Block clicked = player.getTargetBlockExact(6);
            if (clicked == null) return;
            if (BlockUtils.getIgnored().contains(clicked.getType())) {
                player.setGameMode(GameMode.SURVIVAL);
                return;
            }
            ItemStack item = player.getInventory().getItemInMainHand();
            if (!BlockUtils.getIgnored().contains(clicked.getType())
                && (!item.getType().isBlock() && !item.getType().isSolid())) {
                if (!BlockUtils.isStrong(item, clicked)) {
                    player.setGameMode(GameMode.ADVENTURE);
                } else {
                    player.setGameMode(GameMode.SURVIVAL);
                }
            } else {
                player.setGameMode(!item.getType().isAir() && (BlockUtils.getIgnored().contains(clicked.getType())
                        || item.getType().isBlock()
                        || item.getType().isSolid()) ? GameMode.SURVIVAL : GameMode.ADVENTURE);
            }
        }
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
