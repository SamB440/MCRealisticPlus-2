package net.islandearth.mcrealistic.tasks;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

public record WindTask(MCRealistic plugin) implements Runnable {

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            plugin.getCache().getPlayer(player).ifPresent(mcRealisticPlayer -> {
                if (mcRealisticPlayer.isNearWarmthSource()) return;

                if (playWind(player)) {
                    int minY = plugin.getConfig().getInt("Server.Player.Wind.Minimum Y", 150);
                    float windStrength = (float) (((player.getLocation().getY() - minY) / (256 - minY)) * 100);
                    player.playSound(player.getLocation(), Sound.ITEM_ELYTRA_FLYING, windStrength, 1.0F);

                    if (plugin.getConfig().getBoolean("Server.Player.Wind.Push")
                        && Math.random() < 0.5) {
                        Vector direction = new Vector();
                        direction.setX(Math.random()* 2 - 1);
                        direction.setY(0.08);
                        direction.setZ(Math.random() * 2 - 1);
                        direction.normalize();

                        player.setVelocity(direction);
                        player.playSound(player.getLocation(), Sound.ITEM_ELYTRA_FLYING, windStrength * 2, 1.0F);
                    }
                }
            });
        }
    }

    private boolean playWind(Player player) {
        if (!Utils.isWorldEnabled(player.getWorld())) return false;
        if (player.getLocation().getY() < plugin.getConfig().getInt("Server.Player.Wind.Minimum Y", 150)) return false;
        return player.getWorld().getHighestBlockYAt(player.getLocation()) <= player.getLocation().getY();
    }
}
