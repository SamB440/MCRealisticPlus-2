package net.islandearth.mcrealistic.tasks;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import net.islandearth.mcrealistic.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

import java.util.Optional;

public record WarmTask(MCRealistic plugin) implements Runnable {

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!Utils.isWorldEnabled(player.getWorld())) continue;

            if (player.getGameMode() == GameMode.SURVIVAL && !player.isDead()) {
                Optional<MCRealisticPlayer> account = plugin.getCache().getPlayer(player);
                if (account.isEmpty()) return;

                if (!account.get().isWarm()) {
                    int freezeTicks = Math.min(player.getFreezeTicks() + 5, player.getMaxFreezeTicks());
                    player.setFreezeTicks(freezeTicks);
                } else {
                    if (player.getFreezeTicks() != 0) player.setFreezeTicks(player.getFreezeTicks() - 1);
                }
            }
        }
    }
}
