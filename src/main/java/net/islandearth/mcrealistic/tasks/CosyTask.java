package net.islandearth.mcrealistic.tasks;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import net.islandearth.mcrealistic.translation.Translations;
import net.islandearth.mcrealistic.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.block.Block;
import org.bukkit.block.Furnace;
import org.bukkit.block.data.type.Campfire;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Optional;

public class CosyTask implements Runnable {

    private final MCRealistic plugin;
    private Configuration config;
    private ConfigurationSection section;

    private int intervalCounter = 0;

    public CosyTask(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        if (plugin.getConfig().getBoolean("Server.Player.Cosy.DisplayMessage")) {
            config = plugin.getConfig();
            section = config.getConfigurationSection("Server.Player.Cosy");
            if (++intervalCounter != section.getInt("CallInterval")) {
                return;
            }
            intervalCounter = 0;
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (!Utils.isWorldEnabled(player.getWorld())) continue;

                Optional<MCRealisticPlayer> account = plugin.getCache().getPlayer(player);
                if (account.isEmpty()) return;
                account.get().setNearWarmthSource(false);

                if (plugin.getConfig().getBoolean("Server.Player.Cosy.Holding Torch", true)) {
                    if (player.getInventory().getItemInMainHand().getType() == Material.TORCH
                            || player.getInventory().getItemInOffHand().getType() == Material.TORCH) {
                        playerIsCosy(player, section.getInt("TorchDuration"));
                        account.get().setNearWarmthSource(true);
                        continue;
                    }
                }

                double furnaceRadius = section.getInt("FurnaceRadius");
                double torchRadius = section.getDouble("TorchRadius");
                double campfireRadius = section.getDouble("CampfireRadius", 12);
                double inclementWeatherHeatReduction = section.getDouble("InclementWeatherHeatReduction");
                int highestBlockAtPlayer = player.getWorld().getHighestBlockYAt(player.getLocation());

                if (player.getWorld().hasStorm() && player.getLocation().getY() >= highestBlockAtPlayer) {
                    furnaceRadius *= inclementWeatherHeatReduction;
                    torchRadius *= inclementWeatherHeatReduction;
                    campfireRadius *= inclementWeatherHeatReduction;
                }

                double maxRadius = campfireRadius;
                if (furnaceRadius > maxRadius) maxRadius = furnaceRadius;
                if (torchRadius > maxRadius) maxRadius = torchRadius;

                for (Block block :
                        Utils.getNearbyBlocks(player.getLocation(), maxRadius)) {
                    double distanceFromBlock = player.getLocation().distance(block.getLocation());

                    Material blockType = block.getType();
                    if ((blockType == Material.FIRE
                            || blockType == Material.FURNACE)
                            || Tag.CAMPFIRES.isTagged(blockType)
                            || blockType == Material.LAVA) {

                        if (blockType == Material.FURNACE && (distanceFromBlock > furnaceRadius
                                || (section.getBoolean("FurnaceMustBeLit")
                                && ((Furnace) block.getState()).getBurnTime() == 0))) {
                            continue;
                        }

                        if (section.getBoolean("CampfireMustBeLit", true)
                                && Tag.CAMPFIRES.isTagged(blockType)
                                && !((Campfire) block.getBlockData()).isLit()) {
                            continue;
                        }

                        playerIsCosy(player, Tag.CAMPFIRES.isTagged(blockType) ? section.getInt("CampfireDuration", 4) : section.getInt("FurnaceDuration"));
                        account.get().setNearWarmthSource(true);
                        break;
                    } else if (distanceFromBlock <= torchRadius
                            && (blockType.equals(Material.TORCH) || blockType.equals(Material.WALL_TORCH))) {
                        playerIsCosy(player, section.getInt("TorchDuration"));
                        account.get().setNearWarmthSource(true);
                    }
                }
            }
        }
    }

    private void playerIsCosy(Player player, int duration) {
        if (plugin.getColds().contains(player.getUniqueId()) || plugin.getDiseases().contains(player.getUniqueId()))
            return;

        int timeDuration = duration * 60;
        Translations.COSY.send(player);
        int foodLevel = player.getFoodLevel();
        if (section.getInt("FoodLevelForRegeneration") <= foodLevel) {
            int foodDepletion = section.getInt("FoodUseToRecoverHealth");

            player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, timeDuration, 0));
            if (player.getHealth() < 20) {

                PlayerFoodDepletionTask depletionTask = new PlayerFoodDepletionTask(player,
                        foodDepletion * duration);
                int id = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin,
                        depletionTask, 0, timeDuration / (2 * foodDepletion));
                depletionTask.setTaskId(id);
            }
        }
    }
}

