package net.islandearth.mcrealistic.tasks;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import net.islandearth.mcrealistic.translation.Translations;
import net.islandearth.mcrealistic.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Optional;

public record ThirstTask(MCRealistic plugin) implements Runnable {

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!Utils.isWorldEnabled(player.getWorld()) || (player.getGameMode() != GameMode.SURVIVAL
                    && player.getGameMode() != GameMode.ADVENTURE) || player.hasPermission("mcrealistic.thirst.bypass"))
                continue;
            Optional<MCRealisticPlayer> account = plugin.getCache().getPlayer(player);
            if (account.isEmpty()) return;
            int currentThirst = account.get().getThirst();
            int maxThirst = getConfig().getInt("Server.Player.Max Thirst");
            if (currentThirst <= maxThirst && currentThirst > 0) {
                int thirstAddition = Math.min((currentThirst + 10), maxThirst);
                account.get().setThirst(thirstAddition);
                Translations.GETTING_THIRSTY.send(player);
                player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 10, 10));
            }

            if (currentThirst >= getConfig().getInt("Server.Player.Max Thirst")) {
                Translations.REALLY_THIRSTY.send(player);
                player.damage(3.0);
                player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 10, 10));
                player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 10, 10));
                account.get().setFatigue(account.get().getFatigue() + 20);
            }

            if (currentThirst != 0) continue;
            account.get().setThirst(currentThirst + 5);
            Translations.LITTLE_THIRSTY.send(player);
        }
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
