package net.islandearth.mcrealistic.tasks;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import net.islandearth.mcrealistic.translation.Translations;
import net.islandearth.mcrealistic.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.HeightMap;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Optional;

public record MiscTask(MCRealistic plugin) implements Runnable {

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!Utils.isWorldEnabled(player.getWorld())) continue;

            if (player.getGameMode() == GameMode.SURVIVAL && !player.isDead()) {
                Optional<MCRealisticPlayer> account = plugin.getCache().getPlayer(player);
                if (account.isEmpty()) return;
                int currentFatigue = account.get().getFatigue();

                if (player.getHealth() < 6.0 && getConfig().getBoolean("Server.Player.DisplayHurtMessage")) {
                    player.setSprinting(false);
                    player.setSneaking(true);
                    Translations.HURT.send(player);
                }

                if (getConfig().getBoolean("Server.Player.Allow Fatigue") && !player.hasPermission("mcrealistic.fatigue.bypass")) {
                    if (currentFatigue >= getConfig().getInt("Server.Player.Max Fatigue")) {
                        Translations.VERY_TIRED.send(player);
                        player.damage(3.0);
                        player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1200, 1));
                        player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 80, 2));
                    }

                    if (currentFatigue >= getConfig().getInt("Server.Player.Fatigue Tired Range Min")
                            && currentFatigue <= getConfig().getInt("Server.Player.Fatigue Tired Range Max")) {
                        Translations.TIRED.send(player);
                        player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 40, 1));
                    }
                }

                if (!getConfig().getBoolean("Server.Weather.WeatherAffectsPlayer")) continue;

                if (player.getWorld().hasStorm()) {
                    if (player.getInventory().getBoots() != null
                            && player.getInventory().getChestplate() != null) {
                        if (!account.get().hasBrokenBones()) {
                            account.get().setWarm(true);
                        }
                    } else if (player.getInventory().getBoots() == null
                            && player.getInventory().getChestplate() == null
                            && !account.get().isNearWarmthSource() && !account.get().isBurning()) {
                        Block highestBlock = player.getWorld().getHighestBlockAt(player.getLocation(), HeightMap.MOTION_BLOCKING);
                        //TODO check for liquids
                        if (highestBlock.getY() < player.getLocation().getY()) {
                            Translations.COLD.send(player);
                            account.get().setFatigue(account.get().getFatigue() + 10);
                            account.get().setWarm(false);
                            //player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 500, 0));
                            player.damage(3.0);
                            // moved to warmtask
                        } else {
                            account.get().setWarm(true);
                            player.setFreezeTicks(0);
                        }
                    }
                }

                if (!player.getWorld().hasStorm()
                        && !account.get().hasBrokenBones()) {
                    account.get().setWarm(true);
                }

                if (!account.get().isNearWarmthSource()
                        || !player.getWorld().hasStorm() || account.get().hasBrokenBones())
                    continue;

                Translations.COSY.send(player);
                account.get().setFatigue(account.get().getFatigue() - 1);
            }
        }
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }

}
