package net.islandearth.mcrealistic.tasks;

import net.islandearth.mcrealistic.MCRealistic;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public record FatigueTask(MCRealistic plugin) implements Runnable {

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.hasPermission("mcrealistic.fatigue.bypass")) continue;
            plugin.getCache().getPlayer(player).ifPresent(mcRealisticPlayer -> {
                if (mcRealisticPlayer.getFatigue() >= plugin.getConfig().getInt("Server.Player.Max Fatigue")) {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 100,4));
                }
            });
        }
    }
}
