package net.islandearth.mcrealistic.tasks;

import net.islandearth.mcrealistic.MCRealistic;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class TorchTask implements Runnable {

    private final MCRealistic plugin;

    public TorchTask(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            plugin.getCache().getPlayer(player).ifPresent(account -> {
                if (account.isBurning()) player.setFireTicks(20);
            });
        }
    }
}
