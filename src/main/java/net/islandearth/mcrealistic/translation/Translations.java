package net.islandearth.mcrealistic.translation;

import com.convallyria.languagy.api.language.Language;
import com.convallyria.languagy.api.language.key.LanguageKey;
import com.convallyria.languagy.api.language.key.TranslationKey;
import com.convallyria.languagy.api.language.translation.Translation;
import me.clip.placeholderapi.PlaceholderAPI;
import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.api.MCRealisticAPI;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public enum Translations {
    NO_PERMISSION(TranslationKey.of("no_permission")),
    NOT_TIRED(TranslationKey.of("not_tired")),
    TOO_TIRED(TranslationKey.of("too_tired")),
    TIRED(TranslationKey.of("tired")),
    VERY_TIRED(TranslationKey.of("very_tired")),
    NO_HAND_CHOP(TranslationKey.of("no_hand_chop")),
    NOT_THIRSTY(TranslationKey.of("not_thirsty")),
    LITTLE_THIRSTY(TranslationKey.of("little_thirsty")),
    GETTING_THIRSTY(TranslationKey.of("getting_thirsty")),
    REALLY_THIRSTY(TranslationKey.of("really_thirsty")),
    COSY(TranslationKey.of("cosy")),
    COLD(TranslationKey.of("cold")),
    HURT(TranslationKey.of("hurt")),
    HUNGRY(TranslationKey.of("hungry")),
    SHOULD_SLEEP(TranslationKey.of("should_sleep")),
    USED_BANDAGE(TranslationKey.of("used_bandage")),
    LEGS_HEALED(TranslationKey.of("legs_healed")),
    BROKEN_BONES(TranslationKey.of("broken_bones")),
    WAYPOINT_SET(TranslationKey.of("waypoint_set")),
    ITEMS(TranslationKey.of("items")),
    RESPAWN(TranslationKey.of("respawn")),
    CAUGHT_COLD(TranslationKey.of("caught_cold")),
    MEDICINE_TIP(TranslationKey.of("medicine_tip")),
    COLD_DEVELOPED(TranslationKey.of("cold_developed")),
    DISEASE_DAMAGE(TranslationKey.of("disease_damage")),
    SUBSIDE(TranslationKey.of("subside")),
    MEDICINE_NAME(TranslationKey.of("medicine_name")),
    MEDICINE_LORE(TranslationKey.of("medicine_lore")),
    BANDAGE_NAME(TranslationKey.of("bandage_name")),
    BANDAGE_LORE(TranslationKey.of("bandage_lore")),
    CHOCOLATE_MILK_NAME(TranslationKey.of("chocolate_milk_name")),
    CHOCOLATE_MILK_LORE(TranslationKey.of("chocolate_milk_lore")),
    PURIFIED_WATER_NAME(TranslationKey.of("purified_water_name")),
    PURIFIED_WATER_LORE(TranslationKey.of("purified_water_lore")),
    HATCHET_NAME(TranslationKey.of("hatchet_name")),
    HATCHET_LORE(TranslationKey.of("hatchet_lore"));

    private final TranslationKey key;
    private final boolean isList;

    Translations(TranslationKey key) {
        this.key = key;
        this.isList = false;
    }

    public boolean isList() {
        return isList;
    }

    private String getPath() {
        return this.toString().toLowerCase();
    }

    public void send(Player player, Object... values) {
        final Translation translation = MCRealisticAPI.getAPI().getTranslator().getTranslationFor(player, key);
        for (String translationString : translation.colour()) {
            final String send = this.setPapi(player, replaceVariables(translationString, values));
            if (getMessageType() == ChatMessageType.ACTION_BAR) {
                player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(send));
            } else {
                player.sendMessage(send);
            }
        }
    }

    public List<String> get(@Nullable Player player, Object... values) {
        final Translation translation;

        if (player == null) {
            final FileConfiguration config = MCRealisticAPI.getAPI().getTranslator().getHook().getCachedLanguages().get(Language.BRITISH_ENGLISH);
            if (config.isList(key.getKey())) {
                translation = Translation.of(null, Language.BRITISH_ENGLISH, config.getStringList(key.getKey()));
            } else {
                translation = Translation.of(null, Language.BRITISH_ENGLISH, config.getString(key.getKey()));
            }
        } else {
            translation = MCRealisticAPI.getAPI().getTranslator().getTranslationFor(player, key);
        }

        List<String> transformed = new ArrayList<>();
        for (String translationString : translation.colour()) {
            transformed.add(this.setPapi(player, replaceVariables(translationString, values)));
        }
        return transformed;
    }

    public static void generateLang(MCRealistic plugin) {
        File lang = new File(plugin.getDataFolder() + "/lang/");
        lang.mkdirs();

        for (Language language : Language.values()) {
            final LanguageKey languageKey = language.getKey();
            try {
                plugin.saveResource("lang/" + languageKey.getCode() + ".yml", false);
                plugin.getLogger().info("Generated " + languageKey.getCode() + ".yml");
            } catch (IllegalArgumentException ignored) { }

            File file = new File(plugin.getDataFolder() + "/lang/" + languageKey.getCode() + ".yml");
            if (file.exists()) {
                FileConfiguration config = YamlConfiguration.loadConfiguration(file);
                for (Translations key : values()) {
                    if (config.get(key.toString().toLowerCase()) == null) {
                        plugin.getLogger().warning("No value in translation file for key "
                                + key + " was found. Please regenerate or edit your language files with new values!");
                    }
                }
            }
        }
    }

    @NotNull
    private String replaceVariables(String message, Object... values) {
        String modifiedMessage = message;
        for (int i = 0; i < 10; i++) {
            if (values.length > i) modifiedMessage = modifiedMessage.replaceAll("%" + i, String.valueOf(values[i]));
            else break;
        }

        return modifiedMessage;
    }

    @NotNull
    private String setPapi(Player player, String message) {
        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
            return PlaceholderAPI.setPlaceholders(player, message);
        }

        return message;
    }

    public ChatMessageType getMessageType() {
        String configSetting = JavaPlugin.getPlugin(MCRealistic.class).getConfig().getString("Server.Messages.Type");
        if (configSetting == null) return ChatMessageType.CHAT;
        if (configSetting.equals("MESSAGE") || configSetting.equals("CHAT")) {
            return ChatMessageType.CHAT;
        }
        return ChatMessageType.ACTION_BAR;
    }
}
