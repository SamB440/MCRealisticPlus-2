package net.islandearth.mcrealistic.listeners;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.items.Items;
import net.islandearth.mcrealistic.translation.Translations;
import net.islandearth.mcrealistic.utils.TitleManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class ConsumeListener implements Listener {

    private final MCRealistic plugin;
    private final List<World> worlds;

    public ConsumeListener(MCRealistic plugin) {
        this.plugin = plugin;
        this.worlds = plugin.getWorlds();
    }

    @EventHandler
    public void onItemConsume(PlayerItemConsumeEvent pice) {
        final Player player = pice.getPlayer();
        if (!worlds.contains(player.getWorld()) || (player.getGameMode() != GameMode.SURVIVAL && player.getGameMode() != GameMode.ADVENTURE)) return;
        final ItemStack item = pice.getItem();
        plugin.getCache().getPlayer(player).ifPresent(account -> {
            if (item.hasItemMeta() && item.getType().equals(Material.POTION)) {
                PotionMeta pm = (PotionMeta) item.getItemMeta();
                if (pm != null && pm.getBasePotionData().getType() == PotionType.WATER) {
                    if (!getConfig().getBoolean("Server.Player.Thirst.Enabled") || player.hasPermission("mcrealistic.fatigue.bypass")) return;
                    if (getConfig().getBoolean("Server.Player.Thirst.Require_Purify") && item.hasItemMeta()) {
                        final PersistentDataContainer pdc = item.getItemMeta().getPersistentDataContainer();
                        if (!pdc.has(Items.PURIFIED_WATER_KEY, PersistentDataType.BOOLEAN)) {
                            player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 120, 2));
                            player.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 180, 1));
                            return;
                        }
                    }

                    Translations.NOT_THIRSTY.send(player);
                    final int fatigueReduction = account.getFatigue() == 0 ? 0 : account.getFatigue() - 2;
                    account.setFatigue(fatigueReduction);
                    account.setThirst(0);
                    return;
                }

                if (item.hasItemMeta() && item.getItemMeta() != null) {
                    final PersistentDataContainer pdc = item.getItemMeta().getPersistentDataContainer();
                    if (!pdc.has(Items.MEDICINE_KEY, PersistentDataType.BOOLEAN)) return;

                    List<UUID> hasDisease = getDiseases();
                    List<UUID> hasCold = getColds();

                    if (hasCold.contains(player.getUniqueId())) {
                        TitleManager.sendTitle(player, "", Translations.SUBSIDE.get(player, "cold").get(0), 200);
                        hasCold.remove(player.getUniqueId());
                    } else if (hasDisease.contains(player.getUniqueId())) {
                        hasDisease.remove(player.getUniqueId());
                        TitleManager.sendTitle(player, "", Translations.SUBSIDE.get(player, "disease").get(0), 200);
                    }
                }
                return;
            }

            if (item.getType() == Material.MILK_BUCKET && account.hasBrokenBones()) {
                PotionEffect effect = player.getPotionEffect(PotionEffectType.SLOW);
                if (effect != null) {
                    Bukkit.getScheduler().runTaskLater(plugin, () -> player.addPotionEffect(effect), 1L);
                }
            }

            if (isRaw(item.getType())
                    && getConfig().getBoolean("Server.Player.Raw_Food_Illness")) {
                Random random = ThreadLocalRandom.current();
                int randomPoison = random.nextInt(2);
                if (randomPoison == 1) {
                    player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 60, 1));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 120, 0));
                }
            }
        });
    }

    private boolean isRaw(Material material) {
        return material.toString().contains("RAW");
    }

    private List<UUID> getDiseases() {
        return plugin.getDiseases();
    }

    private List<UUID> getColds() {
        return plugin.getColds();
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
