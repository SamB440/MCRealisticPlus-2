package net.islandearth.mcrealistic.listeners;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.gamemode.CustomGameMode;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import net.islandearth.mcrealistic.translation.Translations;
import net.islandearth.mcrealistic.utils.BlockUtils;
import net.islandearth.mcrealistic.utils.ExtraTags;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

public class BlockListener implements Listener {

    private final MCRealistic plugin;
    private final List<World> worlds;
    private final Set<Material> logs;
    private final Set<Material> leaves;
    private final List<Material> buildingIgnore;

    public BlockListener(MCRealistic plugin) {
        this.plugin = plugin;
        this.worlds = plugin.getWorlds();
        this.logs = Tag.LOGS.getValues();
        this.leaves = Tag.LEAVES.getValues();

        List<Material> buildingIgnore = new ArrayList<>();
        for (String string : plugin.getConfig().getStringList("Server.Building.Ignored_Blocks")) {
            buildingIgnore.add(Material.valueOf(string));
        }

        this.buildingIgnore = buildingIgnore;
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent bpe) {
        Player player = bpe.getPlayer();
        Optional<MCRealisticPlayer> account = plugin.getCache().getPlayer(player);
        if (account.isEmpty()) return;
        Block block = bpe.getBlock();
        boolean isInRegion = plugin.getManagers().getIntegrationManager().isInRegion(block.getLocation());

        if (worlds.contains(player.getWorld())
                && player.getGameMode().equals(GameMode.SURVIVAL)
                && !isInRegion) {
            if (getConfig().getBoolean("Server.Player.Allow Fatigue") && !player.hasPermission("mcrealistic.fatigue.bypass")) {
                if (account.get().getFatigue() >= getConfig().getInt("Server.Player.Max Fatigue")
                        && !Tag.BEDS.isTagged(block.getType())) {
                    Translations.TOO_TIRED.send(player);
                    bpe.setCancelled(true);
                    return;
                }

                account.get().setFatigue(account.get().getFatigue() + 1);
            }

            if (getConfig().getBoolean("Server.Building.Realistic_Building")
                    && player.hasPermission("MCRealistic.gravity")) {
                Material l1 = bpe.getBlock().getLocation().subtract(1.0, 0.0, 0.0).getBlock().getType();
                Material l2 = bpe.getBlock().getLocation().subtract(0.0, 0.0, 1.0).getBlock().getType();
                Material l3 = bpe.getBlock().getLocation().add(1.0, 0.0, 0.0).getBlock().getType();
                Material l4 = bpe.getBlock().getLocation().add(0.0, 0.0, 1.0).getBlock().getType();
                Material l5 = bpe.getBlock().getLocation().add(0.0, 1.0, 0.0).getBlock().getType();
                if (Tag.FENCES.getValues().contains(block.getType())) {
                    int amount = 0;
                    if (l1 != Material.AIR) amount++;
                    if (l2 != Material.AIR) amount++;
                    if (l3 != Material.AIR) amount++;
                    if (l4 != Material.AIR) amount++;
                    if (l5 != Material.AIR) amount++;
                    if (amount < 2 && !Tag.FENCES.getValues().contains(bpe.getBlock().getLocation().clone().subtract(0, 1, 0).getBlock().getType())) {
                        Location loc = block.getLocation();
                        loc.setX(loc.getX() + 0.5);
                        loc.setZ(loc.getZ() + 0.5);
                        loc.getWorld().spawnFallingBlock(loc, block.getBlockData());
                        block.setType(Material.AIR);
                    }
                }

                if (player.getInventory().getItemInMainHand().hasItemMeta()) {
                    return;
                } else if (buildingIgnore.contains(bpe.getBlock().getType())) {
                    return;
                } else if (bpe.getBlock().getLocation().subtract(0.0, 1.0, 0.0).getBlock().getType() != Material.AIR) {
                    return;
                } else if (logs.contains(l1) || logs.contains(l2) || logs.contains(l3) || logs.contains(l4) || logs.contains(l5)) {
                    return;
                }

                Location loc = block.getLocation();
                loc.setX(loc.getX() + 0.5);
                loc.setZ(loc.getZ() + 0.5);
                loc.getWorld().spawnFallingBlock(loc, block.getBlockData());
                block.setType(Material.AIR);
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent bbe) {
        Player player = bbe.getPlayer();
        Optional<MCRealisticPlayer> account = plugin.getCache().getPlayer(player);
        if (account.isEmpty()) return;
        Block block = bbe.getBlock();
        CustomGameMode gamemode = CustomGameMode.valueOf(getConfig().getString("Server.GameMode.Type").toUpperCase());
        boolean isInRegion = plugin.getManagers().getIntegrationManager().isInRegion(block.getLocation());

        if (worlds.contains(player.getWorld())
                && player.getGameMode() == GameMode.SURVIVAL
                && !isInRegion) {
            if (getConfig().getBoolean("Server.World.Falling_Trees")) {
                Location above = new Location(block.getWorld(), block.getLocation().getX() + 0.5, block.getLocation().getY() + 1, block.getLocation().getZ() + 0.5);

                for (int i = 0; i < 256; i++) {
                    if (logs.contains(above.getBlock().getType())) {
                        BlockData data = above.getBlock().getBlockData();
                        above.getBlock().setType(Material.AIR);
                        above.getWorld().spawnFallingBlock(above, data);
                        above.setY(above.getY() + 1);
                    } else break;
                }
            }

            if (getConfig().getBoolean("Server.Building.Realistic_Building")
                    && player.hasPermission("MCRealistic.gravity")) {
                Location above = block.getLocation().clone().add(0, 1, 0);
                if (Tag.FENCES.getValues().contains(block.getType())) {
                    for (int i = 0; i < 6; i++) {
                        if (!buildingIgnore.contains(above.getBlock().getType())) {
                            BlockData data = above.getBlock().getBlockData();
                            above.getBlock().setType(Material.AIR);
                            above.getWorld().spawnFallingBlock(above, data);
                            above.setY(above.getY() + 1);
                        } else break;
                    }
                }
            }

            if (gamemode == CustomGameMode.PRE_ADVENTURE) {
                if (!bbe.isCancelled() && leaves.contains(block.getType())) {
                    Random random = new Random();
                    int randomTreeChop = random.nextInt(2);

                    switch (randomTreeChop) {
                        case 0 -> player.getWorld().dropItem(block.getLocation(), new ItemStack(Material.STICK));
                        case 1 -> bbe.setDropItems(false);
                        case 2 -> {
                            player.getWorld().dropItem(block.getLocation(), new ItemStack(Material.STICK));
                            player.getWorld().dropItem(block.getLocation(), new ItemStack(Material.STICK));
                        }
                    }
                }
            }

            if (getConfig().getBoolean("Server.Player.Allow Fatigue") && !player.hasPermission("mcrealistic.fatigue.bypass")) {
                if (account.get().getFatigue() >= getConfig().getInt("Server.Player.Max Fatigue")) {
                    Translations.TOO_TIRED.send(player);
                    bbe.setCancelled(true);
                    return;
                }

                account.get().setFatigue(account.get().getFatigue() + 1);
            }

            if (logs.contains(block.getType())) {
                int damage = getConfig().getInt("Server.Player.Chop With Hands Damage");
                if (damage != 0) player.damage(damage);

                if (!ExtraTags.AXES.getMaterials().contains(player.getInventory().getItemInMainHand().getType()) && !getConfig().getBoolean("Server.Player.Allow Chop Down Trees With Hands")) {
                    Translations.NO_HAND_CHOP.send(player);
                    bbe.setCancelled(true);
                } else if (ExtraTags.AXES.getMaterials().contains(player.getInventory().getItemInMainHand().getType())
                        && getConfig().getBoolean("Server.Player.Trees have random number of drops")) {
                    Random random = new Random();
                    int randomTreeChop = random.nextInt(2);

                    switch (randomTreeChop) {
                        case 0 -> player.getWorld().dropItem(block.getLocation(), block.getState().getData().toItemStack(1));
                        case 1 -> bbe.setDropItems(false);
                        case 2 -> player.getWorld().dropItem(block.getLocation(), block.getState().getData().toItemStack(2));
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onDamage(BlockDamageEvent bde) {
        Player player = bde.getPlayer();
        CustomGameMode gamemode = CustomGameMode.valueOf(getConfig().getString("Server.GameMode.Type").toUpperCase());
        if (gamemode == CustomGameMode.PRE_ADVENTURE
                && (player.getGameMode() == GameMode.SURVIVAL || player.getGameMode() == GameMode.ADVENTURE)) {
            Block clicked = bde.getBlock();
            if (!BlockUtils.getIgnored().contains(clicked.getType())) {
                if (!BlockUtils.isStrong(player.getInventory().getItemInMainHand(), clicked)) {
                    player.setGameMode(GameMode.ADVENTURE);
                    bde.setCancelled(true);
                    return;
                }
            }
            player.setGameMode(GameMode.SURVIVAL);
        }
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
