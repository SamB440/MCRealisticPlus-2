package net.islandearth.mcrealistic.listeners;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.utils.Utils;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;

public class ProjectileListener implements Listener {

    private MCRealistic plugin;

    public ProjectileListener(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onProjectileHit(ProjectileHitEvent event) {
        Projectile arrow = event.getEntity();
        if (Utils.isWorldEnabled(arrow.getWorld())
                && !plugin.getManagers().getIntegrationManager().isInRegion(arrow.getLocation())) {
            if (getConfig().getBoolean("Server.Player.Allow Enchanted Arrow")
                    && arrow.getType() == EntityType.ARROW
                    && arrow.getFireTicks() > 0) {
                Block block = arrow.getWorld().getBlockAt(arrow.getLocation());
                if (block.getType() == Material.AIR) {
                    block.setType(Material.FIRE);
                }
            }
        }
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
