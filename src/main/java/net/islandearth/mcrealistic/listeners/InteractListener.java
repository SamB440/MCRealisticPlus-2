package net.islandearth.mcrealistic.listeners;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.items.Items;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import net.islandearth.mcrealistic.translation.Translations;
import net.islandearth.mcrealistic.utils.Utils;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.potion.PotionEffectType;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class InteractListener implements Listener {

    private final MCRealistic plugin;
    private final List<World> worlds;
    private final Set<Material> leaves;
    private final List<Material> ignore;

    public InteractListener(MCRealistic plugin) {
        this.plugin = plugin;
        this.worlds = plugin.getWorlds();
        this.leaves = Tag.LEAVES.getValues();
        this.ignore = Arrays.asList(Material.GRASS,
                Material.TALL_GRASS,
                Material.SEAGRASS,
                Material.TALL_SEAGRASS,
                Material.FLOWER_POT,
                Material.SUNFLOWER,
                Material.CHORUS_FLOWER,
                Material.OXEYE_DAISY,
                Material.DEAD_BUSH,
                Material.FERN,
                Material.DANDELION,
                Material.POPPY,
                Material.BLUE_ORCHID,
                Material.ALLIUM,
                Material.AZURE_BLUET,
                Material.RED_TULIP,
                Material.ORANGE_TULIP,
                Material.WHITE_TULIP,
                Material.PINK_TULIP,
                Material.BROWN_MUSHROOM,
                Material.RED_MUSHROOM,
                Material.END_ROD,
                Material.ROSE_BUSH,
                Material.PEONY,
                Material.LARGE_FERN,
                Material.REDSTONE,
                Material.REPEATER,
                Material.COMPARATOR,
                Material.LEVER,
                Material.SEA_PICKLE,
                Material.SUGAR_CANE,
                Material.FIRE,
                Material.WHEAT,
                Material.WHEAT_SEEDS,
                Material.CARROTS,
                Material.BEETROOT,
                Material.BEETROOT_SEEDS,
                Material.MELON,
                Material.MELON_STEM,
                Material.MELON_SEEDS,
                Material.POTATOES,
                Material.PUMPKIN,
                Material.PUMPKIN_STEM,
                Material.PUMPKIN_SEEDS);
    }

    @EventHandler
    public void onLeave(PlayerBedLeaveEvent event) {
        final Player player = event.getPlayer();
        if (!Utils.isWorldEnabled(player.getWorld()) || (player.getGameMode() != GameMode.SURVIVAL && player.getGameMode() != GameMode.ADVENTURE)) return;
        Optional<MCRealisticPlayer> account = plugin.getCache().getPlayer(player);
        if (account.isEmpty()) return;

        // Check for fatigue
        if (getConfig().getBoolean("Server.Player.Allow Fatigue")
                && !player.hasPermission("mcrealistic.fatigue.bypass") && account.get().getFatigue() != 0) {
            account.get().setFatigue(0);
            Translations.NOT_TIRED.send(player);
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onInteract(PlayerInteractEvent pie) {
        final Player player = pie.getPlayer();
        if (!Utils.isWorldEnabled(player.getWorld()) || (player.getGameMode() != GameMode.SURVIVAL && player.getGameMode() != GameMode.ADVENTURE)) return;
        Optional<MCRealisticPlayer> account = plugin.getCache().getPlayer(player);
        if (account.isEmpty()) return;

        if (pie.getAction() == Action.RIGHT_CLICK_AIR || pie.getAction() == Action.RIGHT_CLICK_BLOCK) {
            final ItemStack itemStack = player.getInventory().getItemInMainHand();

            // Waypoints
            if (itemStack.getType() == Material.COMPASS) {
                player.setCompassTarget(player.getLocation());
                account.get().setWaypointX(player.getLocation().getBlockX());
                account.get().setWaypointY(player.getLocation().getBlockY());
                account.get().setWaypointZ(player.getLocation().getBlockZ());
                Translations.WAYPOINT_SET.send(player);
                return;
            }

            if (itemStack.getType() != Material.PAPER || !itemStack.hasItemMeta()) return;

            // Check for broken bones
            final ItemMeta itemMeta = itemStack.getItemMeta();
            final PersistentDataContainer pdc = itemMeta.getPersistentDataContainer();
            if (!pdc.has(Items.BANDAGE_KEY, PersistentDataType.BOOLEAN)) return;
            if (account.get().hasBrokenBones()) {
                account.get().setHasBrokenBones(false);
                Translations.USED_BANDAGE.send(player);
                ItemStack item = new ItemStack(itemStack);
                item.setAmount(1);
                player.getInventory().removeItem(new ItemStack(item));
                player.updateInventory();
                player.removePotionEffect(PotionEffectType.SLOW);
            }
        }
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
