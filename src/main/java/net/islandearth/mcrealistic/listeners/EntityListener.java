package net.islandearth.mcrealistic.listeners;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.translation.Translations;
import net.islandearth.mcrealistic.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public record EntityListener(MCRealistic plugin) implements Listener {

    @EventHandler(ignoreCancelled = true)
    public void onPlayerFall(EntityDamageEvent ede) {
        if (ede.getEntity() instanceof Player player) {
            if (player.getGameMode() != GameMode.SURVIVAL && player.getGameMode() != GameMode.ADVENTURE) return;
            plugin.getCache().getPlayer(player).ifPresent(account -> {
                if (!Utils.isWorldEnabled(player.getWorld()) || account.hasBrokenBones()) return;

                if (plugin.getConfig().getBoolean("Server.Player.Broken_Bones.Enabled")) {
                    if (ede.getCause() == EntityDamageEvent.DamageCause.FALL
                            && player.getFallDistance() >= 7.0f
                            && !player.isFlying()) {

                        Translations.BROKEN_BONES.send(player);
                        account.setHasBrokenBones(true);

                        long boneRepairTime = (long) (player.getFallDistance() * 80.0f);
                        player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (int) boneRepairTime, 1));
                        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> {
                            if (account.hasBrokenBones()) {
                                Translations.LEGS_HEALED.send(player);
                                account.setHasBrokenBones(false);
                            }
                        }, boneRepairTime);
                    }
                }
            });
        }
    }
}
