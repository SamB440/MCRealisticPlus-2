/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.listeners;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.translation.Translations;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class ConnectionListener implements Listener {

    private final MCRealistic plugin;

    public ConnectionListener(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent pje) {
        Player player = pje.getPlayer();
        plugin.getCache().getPlayer(player).ifPresent(account -> {
            Bukkit.getScheduler().runTask(plugin, () -> {
                if (!player.hasPlayedBefore()) {
                    account.giveRespawnItems(plugin);
                }

                if (account.getWaypointX() != 0 && account.getWaypointZ() != 0) {
                    player.setCompassTarget(new Location(player.getWorld(), account.getWaypointX(), account.getWaypointY(), account.getWaypointZ()));
                }

                if (account.hasBrokenBones()) {
                    PotionEffect potionEffect = player.getPotionEffect(PotionEffectType.SLOW);
                    if (potionEffect != null) {
                        long boneRepairTime = potionEffect.getDuration();
                        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> {
                            if (account.hasBrokenBones()) {
                                Translations.LEGS_HEALED.send(player);
                                account.setHasBrokenBones(false);
                            }
                        }, boneRepairTime);
                    } else {
                        account.setHasBrokenBones(false);
                    }
                }
            });
        });
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
