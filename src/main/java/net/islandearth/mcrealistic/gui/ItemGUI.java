package net.islandearth.mcrealistic.gui;

import com.github.stefvanschie.inventoryframework.gui.GuiItem;
import com.github.stefvanschie.inventoryframework.gui.type.ChestGui;
import com.github.stefvanschie.inventoryframework.pane.StaticPane;
import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.items.Items;
import net.islandearth.mcrealistic.translation.Translations;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.Collections;

public class ItemGUI extends MCRealisticGUI {

    private ChestGui gui;

    public ItemGUI(MCRealistic plugin, Player player) {
        super(plugin, player);
    }

    @Override
    public void render() {
        this.gui = new ChestGui(1, Translations.ITEMS.get(getPlayer()).get(0));
        StaticPane items = new StaticPane(0, 0, 9, 1);
        int current = 0;
        for (ItemStack item : Items.getItems(getPlayer())) {
            items.addItem(new GuiItem(item), current, 0);
            current++;
        }
        gui.addPane(items);
    }

    @Override
    public ChestGui getGui() {
        return gui;
    }
}
