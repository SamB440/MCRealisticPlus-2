package net.islandearth.mcrealistic.gui;

import com.github.stefvanschie.inventoryframework.gui.type.ChestGui;
import net.islandearth.mcrealistic.MCRealistic;
import org.bukkit.entity.Player;

public abstract class MCRealisticGUI {

    private final MCRealistic plugin;
    private final Player player;

    public MCRealisticGUI(MCRealistic plugin, Player player) {
        this.plugin = plugin;
        this.player = player;
    }

    public MCRealistic getPlugin() {
        return plugin;
    }

    public Player getPlayer() {
        return player;
    }

    public abstract void render();

    public abstract ChestGui getGui();

    public void open() {
        player.closeInventory();
        render();
        getGui().show(player);
    }
}
