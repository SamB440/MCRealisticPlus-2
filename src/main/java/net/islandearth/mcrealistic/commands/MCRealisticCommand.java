package net.islandearth.mcrealistic.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Optional;
import co.aikar.commands.annotation.Subcommand;
import co.aikar.commands.bukkit.contexts.OnlinePlayer;
import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.gui.ItemGUI;
import net.islandearth.mcrealistic.items.Items;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@CommandAlias("mcrealistic|mcr")
public class MCRealisticCommand extends BaseCommand {

    private final MCRealistic plugin;

    public MCRealisticCommand(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @Default
    public void onDefault(CommandHelp help) {
        help.showHelp();
    }

    @Subcommand("reload")
    @CommandPermission("mcr.reload")
    public void onReload(CommandSender sender) {
        sender.sendMessage(ChatColor.GREEN + "Reloading...");
        plugin.reloadConfig();
        plugin.saveConfig();
        sender.sendMessage(ChatColor.GREEN + "Done!");
        sender.sendMessage(ChatColor.YELLOW + "This command does not properly reload the plugin. Before reporting issues, do a full restart.");
    }

    @Subcommand("items")
    @CommandPermission("mcr.items")
    public void onItems(Player player) {
        new ItemGUI(plugin, player).open();
    }

    @Subcommand("item")
    @CommandPermission("mcr.items")
    @CommandCompletion("@items @range:20 @players")
    public void onItem(CommandSender sender, String item, int amount, @Optional OnlinePlayer target) {
        if (target == null && !(sender instanceof Player)) return;
        Player player = target == null ? (Player) sender : target.getPlayer();
        ItemStack itemStack = null;

        switch (item.toLowerCase()) {
            case "medicine" -> itemStack = Items.MEDICINE.apply(player);
            case "bandage" -> itemStack = Items.BANDAGE.apply(player);
            case "chocolatemilk" -> itemStack = Items.CHOCOLATE_MILK.apply(player);
            case "purifiedwater" -> itemStack = Items.PURIFIED_WATER.apply(player);
            case "hatchet" -> itemStack = Items.HATCHET.apply(player);
            default -> sender.sendMessage(ChatColor.RED + "Item not found.");
        }

        if (itemStack == null) {
            return;
        }

        itemStack.setAmount(amount);
        player.getInventory().addItem(itemStack);
    }
}
