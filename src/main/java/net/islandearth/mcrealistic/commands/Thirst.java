package net.islandearth.mcrealistic.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.utils.Utils;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

@CommandAlias("thirst")
public class Thirst extends BaseCommand {

    private final MCRealistic plugin;

    public Thirst(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @Default
    @CommandPermission("mcr.thirst")
    public void onDefault(Player player) {
        if (Utils.isWorldEnabled(player.getWorld())) {
            if (getConfig().getBoolean("Server.Player.Allow /thirst")) {
                plugin.getCache().getPlayer(player).ifPresent(account -> {
                    player.sendMessage(ChatColor.GOLD + "==== " + ChatColor.DARK_GREEN + "My thirst is: "
                            + ChatColor.GREEN + account.getThirst() + "/"
                            + getConfig().getInt("Server.Player.Max Thirst")
                            + ChatColor.GOLD + " ====");
                });
            }
        } else {
            player.sendMessage(ChatColor.RED + "MCRealistic is not enabled in this world.");
        }
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
