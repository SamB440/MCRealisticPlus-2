package net.islandearth.mcrealistic.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.utils.Utils;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

@CommandAlias("fatigue")
public class Fatigue extends BaseCommand {

    private final MCRealistic plugin;

    public Fatigue(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @Default
    @CommandPermission("mcr.fatigue")
    public void onDefault(Player player) {
        if (Utils.isWorldEnabled(player.getWorld())) {
            if (getConfig().getBoolean("Server.Player.Allow /fatigue")) {
                plugin.getCache().getPlayer(player).ifPresent(account -> {
                    final int fatigue = account.getFatigue();
                    final int maxFatigue = getConfig().getInt("Server.Player.Max Fatigue");
                    player.sendMessage(ChatColor.GOLD + "==== " + ChatColor.DARK_GREEN + "My fatigue is: "
                            + ChatColor.GREEN + fatigue + "/"
                            + maxFatigue
                            + ChatColor.GOLD + " ====");
                });
            }
        } else {
            player.sendMessage(ChatColor.RED + "MCRealistic is not enabled in this world.");
        }
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
