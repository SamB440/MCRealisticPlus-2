package net.islandearth.mcrealistic.player;

import net.islandearth.mcrealistic.MCRealistic;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.UUID;

public class PlayerListener implements Listener {

    private final MCRealistic plugin;

    public PlayerListener(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onJoin(AsyncPlayerPreLoginEvent event) {
        try {
            UUID uuid = event.getUniqueId();
            File file = new File(plugin.getDataFolder() + "/players/" + uuid + ".json");
            if (!file.exists()) {
                plugin.getCache().addPlayer(uuid);
            } else {
                Reader reader = new FileReader(file);
                MCRealisticPlayer mcRealisticPlayer = plugin.getGson().fromJson(reader, MCRealisticPlayer.class);
                plugin.getCache().addPlayer(mcRealisticPlayer);
            }
        } catch (Exception e) {
            event.setKickMessage(ChatColor.RED + "MCRealistic could not load your data: " + e.getMessage());
            event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        plugin.getCache().getPlayer(player).ifPresent(account -> {
            Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> account.save(plugin));
            plugin.getCache().removePlayer(player);
        });
    }
}
