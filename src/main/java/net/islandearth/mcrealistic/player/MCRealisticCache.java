package net.islandearth.mcrealistic.player;

import org.bukkit.entity.Player;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class MCRealisticCache {

    private final Map<UUID, MCRealisticPlayer> players = new ConcurrentHashMap<>();

    public void addPlayer(Player player) {
        addPlayer(player.getUniqueId());
    }

    public void addPlayer(UUID uuid) {
        players.put(uuid, new MCRealisticPlayer(uuid));
    }

    public void addPlayer(MCRealisticPlayer player) {
        players.put(player.getUUID(), player);
    }

    public void removePlayer(Player player) {
        players.remove(player.getUniqueId());
    }

    public Optional<MCRealisticPlayer> getPlayer(Player player) {
        return Optional.ofNullable(players.get(player.getUniqueId()));
    }
}
