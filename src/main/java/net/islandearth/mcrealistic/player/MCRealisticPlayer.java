package net.islandearth.mcrealistic.player;

import com.google.gson.Gson;
import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.translation.Translations;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.util.UUID;

public class MCRealisticPlayer {

    private transient MCRealistic plugin;
    private final UUID player;

    private int thirst;
    private int fatigue;
    private int waypointX;
    private int waypointY;
    private int waypointZ;
    private boolean isWarm;
    private boolean nearWarmthSource;
    private boolean hasBrokenBones;
    private boolean burning;

    public MCRealisticPlayer(UUID player) {
        this.plugin = JavaPlugin.getPlugin(MCRealistic.class);
        this.player = player;
        this.isWarm = true;
    }

    public UUID getUUID() {
        return player;
    }

    public int getFatigue() {
        return fatigue;
    }

    public void setFatigue(int fatigue) {
        if (this.plugin == null) this.plugin = JavaPlugin.getPlugin(MCRealistic.class);
        if (fatigue > plugin.getConfig().getInt("Server.Player.Max Fatigue")) {
            return;
        }
        this.fatigue = fatigue;
    }

    public int getThirst() {
        return thirst;
    }

    public void setThirst(int thirst) {
        this.thirst = thirst;
    }

    public int getWaypointX() {
        return waypointX;
    }

    public int getWaypointY() {
        return waypointY;
    }

    public int getWaypointZ() {
        return waypointZ;
    }

    public void setWaypointX(int waypointX) {
        this.waypointX = waypointX;
    }

    public void setWaypointY(int waypointY) {
        this.waypointY = waypointY;
    }

    public void setWaypointZ(int waypointZ) {
        this.waypointZ = waypointZ;
    }

    public boolean isWarm() {
        return isWarm;
    }

    public void setWarm(boolean warm) {
        isWarm = warm;
    }

    public boolean isNearWarmthSource() {
        return nearWarmthSource;
    }

    public void setNearWarmthSource(boolean nearWarmthSource) {
        this.nearWarmthSource = nearWarmthSource;
    }

    public boolean hasBrokenBones() {
        return hasBrokenBones;
    }

    public void setHasBrokenBones(boolean hasBrokenBones) {
        this.hasBrokenBones = hasBrokenBones;
    }

    public boolean isBurning() {
        return burning;
    }

    public void setBurning(boolean burning) {
        this.burning = burning;
    }

    public Player getBukkitPlayer() {
        return Bukkit.getPlayer(player);
    }

    public void giveRespawnItems(MCRealistic plugin) {
        if (plugin.getConfig().getBoolean("Server.Player.Spawn with items")) {
            getBukkitPlayer().getInventory().addItem(new ItemStack(Material.GLASS_BOTTLE));
            getBukkitPlayer().getInventory().addItem(new ItemStack(Material.WOODEN_AXE));
        }

        if (plugin.getConfig().getBoolean("Server.Messages.Respawn")) {
            Translations.RESPAWN.send(getBukkitPlayer());
        }
    }

    public void save(MCRealistic plugin) {
        try {
            File file = new File(plugin.getDataFolder() + File.separator + "players" + File.separator + this.player + ".json");
            Writer writer = new FileWriter(file);
            Gson gson = plugin.getGson();
            gson.toJson(this, writer);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean delete(MCRealistic plugin) {
        try {
            File file = new File(plugin.getDataFolder() + File.separator + "regions" + File.separator + this.player + ".json");
            return Files.deleteIfExists(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
