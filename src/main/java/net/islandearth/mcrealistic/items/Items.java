package net.islandearth.mcrealistic.items;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.translation.Translations;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

public class Items {

    public static final NamespacedKey MEDICINE_KEY = new NamespacedKey(JavaPlugin.getPlugin(MCRealistic.class), "medicine");
    public static final NamespacedKey BANDAGE_KEY = new NamespacedKey(JavaPlugin.getPlugin(MCRealistic.class), "bandage");
    public static final NamespacedKey CHOCOLATE_MILK_KEY = new NamespacedKey(JavaPlugin.getPlugin(MCRealistic.class), "chocolate_milk");
    public static final NamespacedKey PURIFIED_WATER_KEY = new NamespacedKey(JavaPlugin.getPlugin(MCRealistic.class), "purified_water");
    public static final NamespacedKey HATCHET_KEY = new NamespacedKey(JavaPlugin.getPlugin(MCRealistic.class), "hatchet_key");

    public static final Function<@Nullable Player, ItemStack> MEDICINE = player -> {
        ItemStack medicine = new ItemStack(Material.POTION, 2);
        ItemMeta medicinemeta = medicine.getItemMeta();
        medicinemeta.setDisplayName(Translations.MEDICINE_NAME.get(player).get(0));
        medicinemeta.setLore(Translations.MEDICINE_LORE.get(player));
        final PersistentDataContainer pdc = medicinemeta.getPersistentDataContainer();
        pdc.set(MEDICINE_KEY, PersistentDataType.BOOLEAN, true);
        medicine.setItemMeta(medicinemeta);
        return medicine;
    };

    public static final Function<@Nullable Player, ItemStack> BANDAGE = player -> {
        ItemStack bandage = new ItemStack(Material.PAPER);
        ItemMeta bm = bandage.getItemMeta();
        bm.setDisplayName(Translations.BANDAGE_NAME.get(player).get(0));
        bm.setLore(Translations.BANDAGE_LORE.get(player));
        final PersistentDataContainer pdc = bm.getPersistentDataContainer();
        pdc.set(BANDAGE_KEY, PersistentDataType.BOOLEAN, true);
        bandage.setItemMeta(bm);
        return bandage;
    };

    public static final Function<@Nullable Player, ItemStack> CHOCOLATE_MILK = player -> {
        ItemStack chocolatemilk = new ItemStack(Material.MILK_BUCKET);
        ItemMeta chocolatemilkmeta = chocolatemilk.getItemMeta();
        chocolatemilkmeta.setDisplayName(Translations.CHOCOLATE_MILK_NAME.get(player).get(0));
        chocolatemilkmeta.setLore(Translations.CHOCOLATE_MILK_LORE.get(player));
        final PersistentDataContainer pdc = chocolatemilkmeta.getPersistentDataContainer();
        pdc.set(CHOCOLATE_MILK_KEY, PersistentDataType.BOOLEAN, true);
        chocolatemilk.setItemMeta(chocolatemilkmeta);
        return chocolatemilk;
    };

    public static final Function<@Nullable Player, ItemStack> PURIFIED_WATER = player -> {
        ItemStack water = new ItemStack(Material.POTION);
        PotionMeta wm = (PotionMeta) water.getItemMeta();
        wm.setDisplayName(Translations.PURIFIED_WATER_NAME.get(player).get(0));
        wm.setBasePotionData(new PotionData(PotionType.WATER));
        wm.setLore(Translations.PURIFIED_WATER_LORE.get(player));
        final PersistentDataContainer pdc = wm.getPersistentDataContainer();
        pdc.set(PURIFIED_WATER_KEY, PersistentDataType.BOOLEAN, true);
        water.setItemMeta(wm);
        return water;
    };

    public static final Function<@Nullable Player, ItemStack> HATCHET = player -> {
        ItemStack hatchet = new ItemStack(Material.WOODEN_AXE);
        ItemMeta hm = hatchet.getItemMeta();
        hm.setDisplayName(Translations.HATCHET_NAME.get(player).get(0));
        hm.setLore(Translations.HATCHET_LORE.get(player));
        final PersistentDataContainer pdc = hm.getPersistentDataContainer();
        pdc.set(HATCHET_KEY, PersistentDataType.BOOLEAN, true);
        hatchet.setItemMeta(hm);
        return hatchet;
    };

    public static List<ItemStack> getItems(Player player) {
        List<ItemStack> items = new ArrayList<>();
        for (Function<Player, ItemStack> item : getItems()) {
            items.add(item.apply(player));
        }
        return items;
    }

    public static Collection<Function<@Nullable Player, ItemStack>> getItems() {
        return List.of(MEDICINE, BANDAGE, CHOCOLATE_MILK, PURIFIED_WATER, HATCHET);
    }
}
