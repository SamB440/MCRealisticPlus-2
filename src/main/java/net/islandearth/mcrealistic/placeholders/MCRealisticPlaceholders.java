package net.islandearth.mcrealistic.placeholders;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.Optional;

public class MCRealisticPlaceholders extends PlaceholderExpansion {

    private final MCRealistic plugin;

    public MCRealisticPlaceholders(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @Override
    public String onPlaceholderRequest(Player player, String placeholder) {
        if (player == null) return "";
        Optional<MCRealisticPlayer> account = plugin.getCache().getPlayer(player);
        if (!account.isPresent()) return "";
        switch (placeholder.toLowerCase()) {
            case "thirst":
                return String.valueOf(account.get().getThirst());
            case "fatigue":
                return String.valueOf(account.get().getFatigue());
            case "infected":
                return String.valueOf(plugin.getDiseases().contains(player.getUniqueId()) || plugin.getColds().contains(player.getUniqueId()));
        }
        return "";
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }

    @Override
    public String getAuthor() {
        return "SamB440";
    }

    @Override
    public String getIdentifier() {
        return "mcrealistic";
    }

    @Override
    public String getVersion() {
        return plugin.getDescription().getVersion();
    }
}
