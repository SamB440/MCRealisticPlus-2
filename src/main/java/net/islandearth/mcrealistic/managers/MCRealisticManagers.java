package net.islandearth.mcrealistic.managers;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.api.integrations.IntegrationManager;
import net.islandearth.mcrealistic.api.integrations.IntegrationType;

import java.util.logging.Level;

public class MCRealisticManagers {

    private IntegrationManager integrationManager;

    public MCRealisticManagers(MCRealistic plugin) {
        try {
            IntegrationType.valueOf(plugin.getConfig().getString("settings.integration.name").toUpperCase())
                    .get()
                    .ifPresent(integrationManager1 -> integrationManager = integrationManager1);
        } catch (ClassNotFoundException e) {
            plugin.getLogger().log(Level.SEVERE, "Could not find IntegrationManager!", e);
        }
    }

    public IntegrationManager getIntegrationManager() {
        return integrationManager;
    }
}
