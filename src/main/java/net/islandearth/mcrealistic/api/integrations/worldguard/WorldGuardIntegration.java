package net.islandearth.mcrealistic.api.integrations.worldguard;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.api.integrations.IntegrationManager;
import org.bukkit.Location;

import java.util.Set;

public class WorldGuardIntegration extends IntegrationManager {

    public WorldGuardIntegration(MCRealistic plugin) {
        super(plugin);
    }

    @Override
    public boolean isInRegion(Location location) {
        return this.getProtectedRegions(location).size() > 0;
    }

    private Set<ProtectedRegion> getProtectedRegions(Location location) {
        return WorldGuard.getInstance()
                .getPlatform()
                .getRegionContainer()
                .get(BukkitAdapter.adapt(location.getWorld()))
                .getApplicableRegions(BlockVector3.at(location.getX(), location.getY(), location.getZ()))
                .getRegions();
    }
}
