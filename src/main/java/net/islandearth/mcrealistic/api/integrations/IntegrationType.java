package net.islandearth.mcrealistic.api.integrations;

import net.islandearth.mcrealistic.MCRealistic;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Optional;

public enum IntegrationType {
    DEFAULT("none.DefaultIntegration"),
    WORLDGUARD("worldguard.WorldGuardIntegration");

    private final String path;

    IntegrationType(String path) {
        this.path = path;
    }

    public Optional<IntegrationManager> get() throws ClassNotFoundException {
        MCRealistic plugin = JavaPlugin.getPlugin(MCRealistic.class);
        plugin.getLogger().info("Loading IntegrationManager implementation...");
        Class<? extends IntegrationManager> clazz = (Class<? extends IntegrationManager>) Class
                .forName("net.islandearth.mcrealistic.api.integrations." + path);
        IntegrationManager generatedClazz = null;
        try {
            generatedClazz = clazz.getConstructor(MCRealistic.class).newInstance(JavaPlugin.getPlugin(MCRealistic.class));
            plugin.getLogger().info("Loaded IntegrationManager implementation " + clazz.getName() + ".");
        } catch (ReflectiveOperationException e) {
            plugin.getLogger().severe("Unable to load IntegrationManager (" + clazz.getName() + ")! Plugin will disable.");
            e.printStackTrace();
            Bukkit.getPluginManager().disablePlugin(plugin);
        }

        return Optional.ofNullable(generatedClazz);
    }
}
