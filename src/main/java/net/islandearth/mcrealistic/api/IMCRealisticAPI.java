package net.islandearth.mcrealistic.api;

import com.convallyria.languagy.api.language.Translator;

public interface IMCRealisticAPI {

    /**
     * Gets the translator provided by Languagy
     * @return Translator
     */
    Translator getTranslator();
}
