plugins {
    java
    id("com.github.johnrengelman.shadow") version "8.1.1"
}

repositories {
    mavenCentral()
    maven("https://repo.convallyria.com/releases/")
    maven("https://repo.aikar.co/content/groups/aikar/")
    maven("https://hub.spigotmc.org/nexus/content/repositories/public/")
    maven("https://maven.enginehub.org/repo/")
    maven("https://repo.extendedclip.com/content/repositories/placeholderapi/")
}

group = "net.islandearth.mcrealistic"
version = "3.1.8"
description = "MCRealistic"

java {
    toolchain.languageVersion.set(JavaLanguageVersion.of(17))
}

dependencies {
    testImplementation("junit:junit:4.13.2")
    testImplementation("com.github.seeseemelk:MockBukkit-v1.19:2.141.0")
    testImplementation("org.reflections:reflections:0.10.2")

    implementation("co.aikar:acf-paper:0.5.1-SNAPSHOT") // ACF
    implementation("org.bstats:bstats-bukkit:3.0.2") // bStats
    implementation("com.github.stefvanschie.inventoryframework:IF:0.10.11") // inventory framework
    implementation("com.convallyria.languagy:api:3.0.2") {
        exclude("com.convallyria.languagy.libs")
    }

    compileOnly("org.jetbrains:annotations:23.0.0")
    compileOnly("org.spigotmc:spigot-api:1.20.1-R0.1-SNAPSHOT") // spigot
    compileOnly("me.clip:placeholderapi:2.10.4") // PAPI
    compileOnly("com.sk89q.worldguard:worldguard-bukkit:7.0.1-SNAPSHOT") { // worldguard
        exclude("org.bukkit")
    }
}

tasks {
    build {
        dependsOn(shadowJar)
    }

    compileJava {
        options.encoding = "UTF-8"
    }

    processResources {
        filesMatching("plugin.yml") {
            expand("version" to version)
        }
    }

    shadowJar {
        minimize()
        archiveClassifier.set("")

        relocate("com.convallyria.languagy", "net.islandearth.mcrealistic.libs.languagy")
        relocate("co.aikar.commands", "net.islandearth.mcrealistic.libs.acf")
        relocate("co.aikar.locales", "net.islandearth.mcrealistic.libs.acf.locales")
        relocate("org.bstats", "net.islandearth.mcrealistic.libs.bstats")
        relocate("com.github.stefvanschie.inventoryframework", "net.islandearth.mcrealistic.libs.inventoryframework")
    }
}