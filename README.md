<br/>
<div align="center">
  <a href="https://gitlab.com/SamB440/tale-of-kingdoms">
    <img src="http://i.imgur.com/y6rWP08.jpg" alt="Logo">
  </a>

  <p>
    <a href="/issues/new">Report Bug</a>
    | 
    <a href="/issues/new">Request Feature</a>
  </p>

![Build](https://img.shields.io/gitlab/pipeline/SamB440/MCRealisticPlus-2/master)
![Forks](https://img.shields.io/badge/dynamic/json?color=white&label=Forks&query=%24.forks_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F6705396)
![Stargazers](https://img.shields.io/badge/dynamic/json?color=white&label=Stars&query=%24.star_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F6705396)
![Issues](https://img.shields.io/badge/dynamic/json?color=white&label=Issues&query=%24.open_issues_count&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F6705396)
![License](https://img.shields.io/badge/license-GPL-blue)
</div>